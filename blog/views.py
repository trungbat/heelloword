from django.shortcuts import render
from django.views.generic import ListView, DeleteView, UpdateView,DetailView, CreateView
from django.urls import reverse_lazy
from .models import Post


class BlogListView(ListView):
	model = Post
	template_name = 'blog/home.html'


class BlogDetailView(DetailView):
	model = Post
	template_name = 'blog/post_detail.html'

class BlogCreateView(CreateView):
	model = Post
	fields = '__all__'
	template_name = 'blog/post_new.html'

class BlogUpdateView(UpdateView):
	model = Post
	template_name = 'blog/post_edit.html'
	fields = ['title','body']

class BlogDeleteView(DeleteView):
	model = Post
	template_name = 'blog/post_delete.html'
	success_url = reverse_lazy('blog:home')